<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Landing Page - Start Bootstrap Theme</title>
    <link href="css/landing-page.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/demo.css" />
    <link rel="stylesheet" type="text/css" href="css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="css/set2.css" />
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="dist/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="dist/sweetalert.css">


</head>
<body>
<a name="about"></a>
<div class="intro-header">
    <div class="container">
        <div class="row">
            <div>
                <div class="intro-message">
                    <h1>All your messages. One Place.</h1>
                    <div style="display: inline">
                        <img class="icon" height=50px src="img/icon1.png"/>
                        <img class="icon" height=50px src="img/icon2.png"/>
                        <img class="icon" height=50px src="img/icon3.png"/>
                        <img class="icon" height=50px src="img/icon4.png"/>
                        <img class="icon" height=50px src="img/icon5.png"/>
                        <img class="icon" height=50px src="img/icon6.png"/>
                    </div>
                    <h3 >+ More</h3>
                    <form class="subcribeForm"  action="" method="post">
                        
                      <span class="input input--kohana input-group">
                          <input class="input__field input__field--kohana form-control" required name="email" type="email" id="input-29" />
                          <label class="input__label input__label--kohana" for="input-29">
                              <i class="fa fa-fw fa-envelope icon--kohana"></i>
                              <span class="input__label-content input__label-content--kohana">Email</span>
                          </label>
                      </span>
                      <span class="btn-span">
                          <input class="btn" type="submit" name="addEntry" value="Notify me"/>
                      </span>
                    </form>
                    <img class="mockup" height="100%" width="80%" src="img/mockup.png"/>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="js/classie.js"></script>
		<script>
			(function() {
				// trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
				if (!String.prototype.trim) {
					(function() {
						// Make sure we trim BOM and NBSP
						var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
						String.prototype.trim = function() {
							return this.replace(rtrim, '');
						};
					})();
				}

				[].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
					// in case the input is already filled..
					if( inputEl.value.trim() !== '' ) {
						classie.add( inputEl.parentNode, 'input--filled' );
					}

					// events:
					inputEl.addEventListener( 'focus', onInputFocus );
					inputEl.addEventListener( 'blur', onInputBlur );
				} );

				function onInputFocus( ev ) {
					classie.add( ev.target.parentNode, 'input--filled' );
				}

				function onInputBlur( ev ) {
                    if( ev.target.value.trim() === '' ) {
						classie.remove( ev.target.parentNode, 'input--filled' );
					}
				}
			})();
		</script>
<script>
    function triggerAlert(){
        swal("Thanks!", "Your email address has been saved!", "success");
    };
</script>

<?php
if(isset($_POST['addEntry'])){
    $out = fopen('emails.csv', 'a');
    fputcsv($out, array($_POST['email']), "\n");
    fclose($out);
    echo '<script type="text/javascript">'
    , 'triggerAlert();'
    , '</script>';
    echo 'what up';
    unset($_POST['addEntry']);

}

?>



</body>

</html>
